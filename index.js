// Server initializations
const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3002;

// Mongoose connect to db
mongoose.connect("mongodb+srv://nathanieljaen12:admin123@zuitt-bootcamp.vs10cer.mongodb.net/s35-a?retryWrites=true&w=majority", 
	{
		// allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Create Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String,
});

const User = mongoose.model("User", userSchema);


// Post unique username, if existing; deny post
app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}).then((result, err) => {

	if(result != null && result.username == req.body.username){
		return res.send("User already existing in the database, enter a new one");
	}else{
		let newUser = new User ({
			username: req.body.username,
			password: req.body.password
		});
		newUser.save().then((saveUser, saveErr) => {
			if(saveErr){
				return console.error(saveErr);
			}else{
				return res.status(201).send("New user registered")
			}
		})
	}
	})
});

// Get all the users in the db
app.get("/users", (req, res)=>{
	User.find({}).then((result, err) =>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
});

// Delete all users in the db
app.delete("/removeAll", (req, res) => {
	User.deleteMany({}).then(res.send("Removed All Users"))
});


app.listen(port,() => console.log(`Server running at port ${port}`));